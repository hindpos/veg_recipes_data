import requests
import os
import json
from lxml import html, etree
from lxml.html.clean import clean_html

#GLOBALS
genre_urls = []

#URL SNIPPETS
url_snippet = { "veg_recipes_base" : "http://www.vegrecipesofindia.com/", "index" : "recipes-index/"}

#PATH CONSTANTS
PARENT_DIR = ".."
TEMP_DIR = os.path.abspath("temp/")

BROWSER_HEADERS = {'user-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:51.0) Gecko/20100101 Firefox/51.0'}

def makedir(dirpath):
    if not (os.path.exists(dirpath)):
        os.mkdir(dirpath)

def checkedchangedir(dirpath):
    makedir(dirpath)
    os.chdir(dirpath)

def get_genres_urls():
    page = requests.get(url_snippet["veg_recipes_base"] + url_snippet["index"], headers = BROWSER_HEADERS)
    tree = html.fromstring(clean_html(page.content))
    return list(tree.xpath("//ol/li/a/@href"))

def get_recipe_urls(genre_url):
    page = requests.get(genre_url, headers = BROWSER_HEADERS)
    print genre_url
    tree = html.fromstring(clean_html(page.content))
    url_list = list(tree.xpath("//div[@class = 'entry-content']//a[starts-with(@href, 'http://www.vegrecipesofindia') or starts-with(@href, 'https://www.vegrecipesofindia')][not(contains(@href, 'jpg') or contains(@href, 'png'))]/@href"))
    url_list = list(set(url_list))
    url_list[:] = [url for url in url_list if not url in genre_urls]
    #url_list =  list(tree.xpath("//a[contains(@strong, .)][contains(@href, 'www.vegrecipesofindia')][not(contains(@href, 'jpg') or contains(@href, 'png'))]/@href"))
    #url_list.append(list(tree.xpath("//strong/a[contains(@href, 'www.vegrecipesofindia')][not(contains(@href, 'jpg') or contains(@href, 'png'))]/@href")))
    return url_list

def get_all_recipes_urls():
    global genre_urls
    genre_urls = get_genres_urls();

    recipe_urls = []
    for genre in genre_urls:
        list_to_append = get_recipe_urls(genre)
        recipe_urls.extend(list_to_append)
        print len(list_to_append)
    recipe_urls = list(set(recipe_urls))

    makedir(TEMP_DIR)

    with open(TEMP_DIR + "/recipe_urls.json", 'w') as f:
        f.write(json.dumps({"urls" : recipe_urls}, indent = 2))

if __name__ == "__main__":
    get_all_recipes_urls()
