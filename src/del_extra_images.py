import os
import json
DIR_FINAL = os.path.abspath("./final") + "/"
DIR_IMG = DIR_FINAL + "img/"
DIR_REQ = DIR_FINAL + "req_img/"
DIR_JSON = DIR_FINAL + "json/"

if not (os.path.exists(DIR_REQ)):
    os.mkdir(DIR_REQ)
json_f_list = os.listdir(DIR_JSON)
for json_f in json_f_list:
    fp = open(DIR_JSON + json_f)
    image_id = str(json.loads(fp.read())["Image ID"])
    os.rename("%s/%s" % (DIR_IMG, image_id), "%s/%s" % (DIR_REQ, image_id))
